<?php 

class UpLoadToBQ
{
	/*
	 * Author Raghav
	 * Utility functions for Google Big Query 
	 * 
	 * 
	 */
	
	
public $date = '';
public $end_date = '';
public $conn;
public $directory='';
public $wait=0;
public $username='';
public $password='';
public $host;
public $lag=0;
public $error='';
public $dataset;

function __construct($date1,$date2,$path,$hostname,$username,$pass,$dataset){
	  
	  $this->date=$date1;
      $this->end_date=$date2;	 
	  $this->directory=$path;
	  $this->wait=1;
	  $this->username=$username;
	  $this->password=$pass;
	  $this->host=$hostname;
	  $this->lag=300;
	  $this->dataset=$dataset;
	  
	  $this->conn=mysql_connect($hostname,$username,$pass);
	  if ($this->conn) {
	  	//die('Not connected : ' . mysql_error());
	  	mysql_select_db('trulyamdly_prod');
	  }
	  
	  date_default_timezone_set('UTC');
	  
}
public function DumpFiveMinutesTables($date)
{
	//This function dumps data from all the tables
	$output=shell_exec("/root/google-cloud-sdk/bin/bq ls --format=json tidal-surf-841:action_log  2>&1");
	$tablenames=array();
	$response_array=json_decode($output,true);
	foreach ($response_array as $val)
	{
	    
	     if(substr_count($val['tableId'],'_')==6)
	     {
	     	$datearr=explode('_',$val['tableId']);	     	
	     	$tabledate=strtotime($datearr[2].'/'.$datearr[3].'/'.$datearr[4]);	     	
	     	if(time()-$tabledate>86400)
	     	{
	     		$tablesnames[]=$val['tableId'];
	     	}
	     }
	}
	foreach ($tablenames as $tablename)
	{
		
	}
	var_dump($tablenames);
	
}

public function GenerateCSV2($i,$date,$hour,$hour1,$tablename)
{
	$sqlquery='';
	if($i==24)
	{
		$date1 = date ("Y-m-d", strtotime("+1 day", strtotime($this->date)));
		$sqlquery=("SELECT ifnull( user_id  , '') , ifnull( user_agent , '') , ifnull( ip  , '') , ifnull( activity, '') , ifnull( event_type, '') ,    ifnull( time_taken , '') , ifnull( event_status , '') , ifnull( trim(both '\"' from event_info) , '') , ifnull( source  , '') , ifnull( admin_id, '') , ifnull( tstamp  , '') , ifnull( device_id , '') , ifnull( sdk_version  , '') , ifnull( os_version , '')  INTO OUTFILE '".$this->directory."action_log_".$tablename."~$i"."'   FIELDS TERMINATED BY '\t'     FROM trulymadly_prod.action_log_new where  tstamp >='".$date." $hour"."' and tstamp < '".$date1." $hour1 '".";");
		//echo($date1);
	
	}
	else
	{
			
		$sqlquery=("SELECT ifnull( user_id  , '') , ifnull( user_agent , '') , ifnull( ip  , '') , ifnull( activity, '') , ifnull( event_type, '') ,    ifnull( time_taken , '') , ifnull( event_status , '') , ifnull( trim(both '\"' from event_info) , '') , ifnull( source  , '') , ifnull( admin_id, '') , ifnull( tstamp  , '') , ifnull( device_id , '') , ifnull( sdk_version  , '') , ifnull( os_version , '')  INTO OUTFILE '".$this->directory."action_log_".$tablename."~$i"."'   FIELDS TERMINATED BY '\t'     FROM trulymadly_prod.action_log_new where  tstamp >='".$date." $hour"."' and tstamp < '".$date." $hour1 '".";");
	
	}
	$result=mysql_query($sqlquery,$this->conn);
	//echo('Zipping File Now'.$tablename);
	$output=shell_exec("gzip ".$this->directory."action_log_".$tablename."~".$i);
	//echo($output.PHP_EOL.PHP_EOL);
	//$myarray[]=$this->directory."action_log_".$tablename."~".$i.'.gz';
	//$mytablename=explode('~',basename($myarray[($i-1)]));
	echo ($sqlquery.PHP_EOL.PHP_EOL);
	$output1=shell_exec("/root/google-cloud-sdk/bin/bq --nosync  load --source_format=CSV  --max_bad_records=30000 -F '\t'  action_log.action_log_$tablename ".$this->directory."action_log_".$tablename."~".$i." user_id:integer,user_agent:string,ip:string,activity:string,event_type:string,time_taken:float,event_status:string,event_info:string,source:string,admin_id:integer,tstamp:timestamp,device_id:string,sdk_version:string,os_version:string 2>&1");
	unlink($this->directory."action_log_".$tablename."~".$i);
}


public function GenerateCSV1($date)
{
	$dir= dirname ( __FILE__ );
	$myarray=array();
	$iTimestamp = mktime(0,0,0,1,1,2011);
	for ($i = 1; $i <= 24; $i++) {
		$hour=date('H:i:s', $iTimestamp);
		$iTimestamp += 3600;
		$hour1=date('H:i:s', $iTimestamp);
		$sqlquery='';
		$tablename=str_replace('-','_',$date);				
		$output=exec("nohup php ".$dir."/multithreaded.php ".$i." ".$date." ".$hour." ".$hour1." ".$tablename." ".$this->directory." ".$this->host." ".$this->username." ".$this->password." "."$this->dataset". ">> /dev/raghav 2>&1 & echo $!");
		$myarray[]=$output;
		echo $output.PHP_EOL;
		//$this->GenerateCSV2($i,$date,$hour,$hour1,$tablename);
		
		
	}
	//var_dump($myarray);
	return $myarray;
	
	
	
}

public function CheckIfFinished($myarray)
{
	echo("CheckIfFinished".PHP_EOL);
	sleep(60*$this->wait);
	
	while($this->checkprocesses($myarray))	{
		echo("CheckIfFinished While ".PHP_EOL);
		sleep(60*$this->wait);
	}
	//return true;
}

public function checkprocesses($myarray)
{
	
	foreach ($myarray as $items)
	{
		if(file_exists("/proc/$items"))
		{
			return true;
		}
	}
	return false;
}



public function HardCode()
{
	echo('Entering Hourly HardCoded');
	$tablename=str_replace('-','_',$this->date);
	$sqlquery=("SELECT ifnull( user_id  , '') , ifnull( user_agent , '') , ifnull( ip  , '') , ifnull( activity, '') , ifnull( event_type, '') ,    ifnull( time_taken , '') , ifnull( event_status , '') , ifnull( trim(both '\"' from event_info) , '') , ifnull( source  , '') , ifnull( admin_id, '') , ifnull( tstamp  , '') , ifnull( device_id , '') , ifnull( sdk_version  , '') , ifnull( os_version , '')  INTO OUTFILE '".$this->directory."action_log_".$tablename."'   FIELDS TERMINATED BY '\t'     FROM trulymadly_prod.action_log_new  where tstamp >='".$this->date." 00:00"."' and tstamp < '".$this->date." 01:00';");
	
	//echo $sqlquery;
	$result=mysql_query($sqlquery,$this->conn);
	$output1=shell_exec("gzip ".$this->directory."action_log_".$tablename);
	$output=shell_exec("/root/google-cloud-sdk/bin/bq load --source_format=CSV  --max_bad_records=30000 -F '\t'  action_log.action_log_$tablename ".$this->directory.""."action_log_".$tablename.".gz"." user_id:integer,user_agent:string,ip:string,activity:string,event_type:string,time_taken:float,event_status:string,event_info:string,source:string,admin_id:integer,tstamp:timestamp,device_id:string,sdk_version:string,os_version:string 2>&1");
	
	echo $output1.PHP_EOL;
	echo $output;
}
public function StarBucks()
{
	$count=4698;
	//$date=date('14-01-2016');
	for($i=1;$i<=4698;$i++)
	{
	  mysql_query("insert into trulymadly_prod.starbucks(coupon) values ($i)",$this->conn);
	}
	//Delhi,bombay,bangalore,chennai, hyd ,pune
	$cities=array('10','20','30','40','50','60');
	$counter=1;
	for($i=1;$i<=4698;$i++)
	{
		$selected_city=intval($counter)-1;
		mysql_query("update trulymadly_prod.starbucks set city='$cities[$selected_city]' where coupon='$i'",$this->conn);
		if($i%6==0)
		{
			$counter=0;
		}
		$counter++;
		
	}
	
	//Now date distribution
	//For 40,50,60
	$inidate=strtotime('22-01-2016');
	$enddate=strtotime('15-02-2016');
	
	$days_between = ceil(abs($enddate - $inidate) / 86400);
	echo $days_between;
	foreach ($cities as $city)
	{
		if($city=='40'||$city=='50'||city=='60')
		{
			$res=mysql_query("select city  from trulymadly_prod.starbucks where city='$city'",$this->conn);
			$status = mysql_num_rows($res);
		}
		
	}
	
	
	
	
}


public function GenerateCSV()
{
	$counter=0;
	
	//for previous data
	try {

		while (strtotime($this->date) <= strtotime($this->end_date)) {
			$result = mysql_query("SELECT count(*) from trulymadly_prod.gbq_log where date='".$this->date."' and tablename='".$this->dataset."';",$this->conn);
			$count=intval(mysql_result($result, 0));
			echo('This is the count '.$count.PHP_EOL);
			if($count==0)
			{
			   	$myarray=$this->GenerateCSV1($this->date);
				//var_dump($myarray);
				echo('Waiting for the processes to be finished for '.$this->date.PHP_EOL);
				$this->CheckIfFinished($myarray);
				//Log into Mysql Own table
				$insertquery="insert into trulymadly_prod.gbq_log(date,tablename) values('".$this->date."','".$this->dataset."');";
				$result=mysql_query($insertquery,$this->conn);
				echo($insertquery.PHP_EOL.mysql_error());
				
				
			}
			else 
			{
				echo('OOPS, the data has already been created for this date;'.PHP_EOL);
			}
			$this->date=date('Y-m-d',strtotime("+1 day",strtotime($this->date)));
			
		}


	}
	catch (Exception $ex)
	{
		echo $ex->getMessage();
		mysql_close($this->conn);
	}
	mysql_close($this->conn);
}

//This function performs uploads from the start date to end date, in the given directory
public function UpLoad($startdate,$enddate)
{
	   try {
	   	
	   
	    echo('Entering Upload With '.$startdate.' and EndDate as : ' .$enddate.PHP_EOL);
		while (strtotime($startdate) <= strtotime($enddate)) {
			
		  
		    $tablename=str_replace('-','_',$startdate);
		    echo('Before Uploading '.$tablename.' '.PHP_EOL);
		    $output=shell_exec("/root/google-cloud-sdk/bin/bq  load --source_format=CSV  --max_bad_records=30000 -F '\t'  action_log.action_log_$tablename ".$this->directory."action_log_".$tablename.'.gz'." user_id:integer,user_agent:string,ip:string,activity:string,event_type:string,time_taken:float,event_status:string,event_info:string,source:string,admin_id:integer,tstamp:timestamp,device_id:string,sdk_version:string,os_version:string 2>&1");
		    echo('Before Uploading '.$tablename.' '.PHP_EOL);		     
		    echo $output.PHP_EOL.PHP_EOL;
		   
			$startdate = date ("Y-m-d", strtotime($startdate.' +1 day'));
			
			
			unlink($this->directory.'action_log_'.$tablename.'.gz');
		}
	  }
	  catch (Exception $ex)
	  {
	  	echo 'Exception Ouccured '.$ex->getMessage();
	  }
	
}


public function LeftOverDump()
{
	echo('Uploading from LeftOverDump'.PHP_EOL);
	$files = scandir($this->directory);
	
	foreach($files as $file)
	{
		if($file != '.' && $file != '..')
		{
			echo('Uploading LeftOver File '.basename($file).PHP_EOL.PHP_EOL);
			$filearr=explode('.',$file);
			$output=shell_exec("/root/google-cloud-sdk/bin/bq load --source_format=CSV  --max_bad_records=30000 -F '\t'  action_log.$filearr[0] ".$this->directory."".$file.""." user_id:integer,user_agent:string,ip:string,activity:string,event_type:string,time_taken:float,event_status:string,event_info:string,source:string,admin_id:integer,tstamp:timestamp,device_id:string,sdk_version:string,os_version:string 2>&1");
			echo $output.PHP_EOL.PHP_EOL;
			unlink($file);
		}
	}
}




public function UploadDumped($filepath)
{
	
	$basefilename=basename($filepath,".txt");
	$tablename=str_replace('-','_',$basefilename);
	$str=file_get_contents($filepath);
	$newfilepath=$filepath.'_gbq';
	$str=str_replace('\N','',$str);
	
	$file=fopen($newfilepath,"w");
	fwrite($file,$str);
	fclose($file);   
	
    $myarr=pathinfo($newfilepath);
    $output=shell_exec("/root/google-cloud-sdk/bin/bq --nosync load --source_format=CSV --max_bad_records=3000 -F '\t' action_log.$tablename ". $newfilepath." user_id:integer,user_agent:string,ip:string,activity:string,event_type:string,time_taken:float,event_status:string,event_info:string,source:string,admin_id:integer,tstamp:timestamp,device_id:string,sdk_version:string,os_version:string 2>&1");
    
    //echo $newfilepath.'new '.PHP_EOL;
    rename($newfilepath,$myarr['dirname'].'/google_biq_query_dumps/'.$tablename.'.txt_gbq');
  	
	
}

function IsNullOrEmptyString($question){
	//returns true when string is null or empty
	return (!isset($question) || trim($question)==='');
}
public function SlaveStatus()
{
	
	//Returns true if Slave Lags
	
	$result=mysql_query("show slave status;",$this->conn);
	$status = mysql_fetch_assoc($result);	
	
	if(!$this->IsNullOrEmptyString($status['Last_IO_Error']))
	{
		echo('Last IO Error Not Empty'.PHP_EOL);
		$this->error=$status['Last_IO_Error'].' With a time lag of '.$status['Seconds_Behind_Master'];
		return true;
		
		
	}
	else 
	{
		echo('Checking Server Lags'.PHP_EOL);
		$this->error='The Server lags '.$status['Seconds_Behind_Master'].' Behind Master';		
		if(!isset($status['Seconds_Behind_Master'])||(intval($status['Seconds_Behind_Master']))>$this->lag)
		{
			echo('Checking Server Lags?YEs it does'.PHP_EOL);
			return true;
		}
		else
		{
			echo('Checking Server Lags?No it doesnt'.PHP_EOL);
			return false;
		}
	}
	
	
 	
	
	
}



public function UploadToGBQFiveMinutes()
{
	$dir = dirname ( __DIR__ ).'/trulymadly/action_log/google_biq_query_dumps/';
	$files = scandir($dir);
	
	foreach($files as $file)
	{
		if(basename($file) != '.' && basename($file) != '..'&&basename($file)!='.gitignore')
		{
			
			$tablename=explode('.',$file);
			$newtablename=explode('_',$tablename[0]);
			//var_dump($newtablename);
			$output=shell_exec("/root/google-cloud-sdk/bin/bq --nosync load --source_format=CSV  --max_bad_records=30000 -F '\t'  action_log.".$newtablename[0].'_'.$newtablename[1].'_'.$newtablename[2].'_'.$newtablename[3].'_'.$newtablename[4]." $file  user_id:integer,user_agent:string,ip:string,activity:string,event_type:string,time_taken:float,event_status:string,event_info:string,source:string,admin_id:integer,tstamp:timestamp,device_id:string,sdk_version:string,os_version:string 2>&1");
			echo $output.PHP_EOL.PHP_EOL;
			//unlink($file);
		}
		
	}
	
}

public  function sendEmail($to, $from, $subject, $message,$ishtml=false){
	$headers = "From: $from \r\n";
	$message=$this->error;
	if($ishtml=="true"){
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		mail($to,$subject,$message,$headers);
	}
	else{
		mail($to,$subject,$message,$headers);
	}
}

}




if($argv[1]=="generate")
{
	$instance=new UpLoadToBQ($argv[2],$argv[3],$argv[4],$argv[5],$argv[6],$argv[7],$argv[8]);
	
	//echo $argv[1];
	$instance->GenerateCSV();
	//$instance->HardCode();
	//$instance->LeftOverDump();
	echo('Work Done'.PHP_EOL);
	//echo(date('Y-m-d',strtotime($argv[3] . ' -3 day')));
	//$instance->UpLoad(date('Y-m-d',strtotime($argv[3] . ' -3 day')),$argv[3]);
}
else if($argv[1]=="daily")
{
	$today=date('Y-m-d',time());
	$yesterday=date('Y-m-d',strtotime("-1 day",strtotime($today)));	
	$instance=new UploadToBQ($yesterday,$yesterday,$argv[2],$argv[3],$argv[4],$argv[5],$argv[6]);
	/*!$instance->SlaveStatus()*/
	if(true)
	{
		$instance->GenerateCSV();
		$instance->sendEmail('raghav@trulymadly.com,shashwat@trulymadly.com','raghav@trulymadly.com','Uploading Complete Without Errors on '.$yesterday,'');
		echo('Work Done'.PHP_EOL);
	}
	else
	{
		//Send a mail to someone warning someone		
		$instance->sendEmail('backend@trulymadly.com','raghav@trulymadly.com','Slave Lagging for Big Query Replication','');
		echo('Mail Sent Due to replication error '.PHP_EOL);
	}
	
	
}
else if($argv[1]=="star")
{
	$instance=new UpLoadToBQ($argv[2],$argv[3],$argv[4],$argv[5],$argv[6],$argv[7],$argv[8]);
	$instance->StarBucks();
}




//$instance=new UpLoadToBQ('2015-12-06','2015-12-09','/home/raghav/script_collection/','','','');////
//$instance->UploadToGBQFiveMinutes();
?>
