#add an import at the top
import boto.ec2
import boto.sns 
import boto.ec2.elb
import logging
import sys
import os
import time
from django.db.models.query import InstanceCheckMeta
import boto.ec2.cloudwatch 
from boto.sns import connect_to_region 
from boto.ec2.cloudwatch import connect_to_region 
from boto.ec2.cloudwatch import MetricAlarm
from fabric.api import *
from ConfigParser import ConfigParser

""" Reading from configuration file !!! """
config = ConfigParser()
config.read ('config.ini')

''' Defining environment variables !!! '''
env.aws_region = config.get('aws' ,'aws_region')
env.user = config.get('aws' ,'user')
env.key_filename = config.get('aws' ,'key_path')
login_user = config.get('aws' ,'user')
key_file = config.get('aws' ,'key_path')
env.timeout = config.get('aws' ,'timeout')
key_name =  config.get('aws' ,'key_name')
hosts = []

''' Cloud watch Global Variables for sns used in sns_subscription !! '''
AWS_KEY = os.environ.get("AWS_ACCESS_KEY_ID")
AWS_SECRET = os.environ.get("AWS_SECRET_ACCESS_KEY")
AWS_REGION = os.environ.get("AWS_EC2_REGION", "ap-southeast-1")
TOPIC = config.get('aws','topicarn')

''' Used for sns mailing  '''
boto.set_stream_logger('boto')

''' Establishing Ec2 connection  '''
def get_ec2_connection():
    if 'ec2' not in env:
        conn = boto.ec2.connect_to_region(env.aws_region)
        if conn is not None:
            env.ec2 = conn
            print "Connected to EC2 region %s" % env.aws_region
        else:
            msg = "Unable to connect to EC2 region %s"
            raise IOError(msg % env.aws_region)
    return env.ec2

''' TO create a instance in specified Region  '''
# i_type ===> for instance type "mysql,Redis,WWW "
def provision_instance(type,i_type, timeout=60, interval=5,wait_for_running=True):
    wait_val = int(interval)
    timeout_val = int(timeout)
    conn = get_ec2_connection()
    global key_name
    key_local=key_name
    
    if config.has_option(i_type ,'key_name'): 
        key_local=config.get(i_type ,'key_name')        
    
    reservations = conn.run_instances(
        config.get(i_type ,'image_id'),
        instance_type= config.get(i_type ,'instance_type'),
        key_name=key_local,
        security_group_ids= [config.get(i_type ,'security_group_id'), ],
        subnet_id = config.get(i_type ,'subnet_id')
    )
    new_instances = [i for i in reservations.instances if i.state == u'pending']
    running_instance = []
    if wait_for_running:
        waited = 0
        while new_instances and (waited < timeout_val):
            time.sleep(wait_val)
            waited += int(wait_val)
            for instance in new_instances:
                state = instance.state
                print "Instance %s is %s" % (instance.ip_address, state)
                if state == "running":
                    running_instance.append(
                        new_instances.pop(new_instances.index(i))
                    )
                instance.update()
    
    instance.add_tag("Name",config.get(i_type ,'instance_name'))
    #attachEBS(instance,config.get(i_type ,'ebs_size'))
    mount(instance,type,config.get(i_type ,'ebs_size'))
    sym_link(instance,type)
    sns_subscription(instance)
    return instance

    ''' Returns Instance type object of specific private ip '''
def get_instance():
    private_ip = raw_input()
    conn = get_ec2_connection()
    
    # Returns a 2D matrix of instances !!
    reservations = conn.get_all_instances(filters= {'private_ip_address':private_ip} )
    
    instance = reservations[0].instances[0]
    if instance is not None:
        print instance
    else :
        print "Unable to get instance"
    return instance

def attachEBS(instance,size,attachment_name=None,timeout=60):
        '''Attach the default-sized EBS volume to that instance'''
        conn = get_ec2_connection()
        # Create a volume in the same availability zone as the instance
        vol = conn.create_volume(size, instance.placement,None ,'gp2')
        print "vol id %s" % vol.id
        print "vol status %s " % vol.status
        
        while (vol.status != 'available'):
                print "Volume %s is %s " % (vol.id , vol.status)
                vol.update()
                time.sleep(10)

        if attachment_name==None :
            attachment_name = "/dev/sdf"
        
        ''' Last Charater of Attachment Name !! '''
        character = attachment_name[-1:] 
        
        '''  Trying with different character untill disk is not mounted !!! '''
        while True:
            try:
                conn.attach_volume (vol.id, instance.id, attachment_name)
                break
            except Exception ,e:
                if character == 'z':
                    break
                character = chr(ord(character)+1)
                attachment_name = "/dev/sd"+character
        
        ''' Wwaiting for ebs to come in use !!  '''
        elapsed = 0
        while elapsed < timeout:
                time.sleep(10)
                elapsed += 10
                vol.update()
                print "Volume %s is %s " % (vol.id , vol.status)
                if vol.status == "in-use":
                        break

        if elapsed >= timeout:
                sys.stderr.write("Timeout when trying to attach volume!\n")
                sys.exit(1)
                
def mount(instance,i_type,ebs_size,vol_name='/mnt'):
        with settings(host_string=instance.private_ip_address,user = login_user , key_filename = key_file):
            counter=0;
            ''' Retrying Again and Again  '''
            while True:
                try:
                    temp = run("ls /sys/block |grep x[a-zA-Z]")
                    if counter>=10:
                        break
                    break
                except Exception, e:
                    counter=counter+1
                    print "instance not yet reachable, wait a little bit", e
                    time.sleep(10)
               
                
            attachEBS(instance,ebs_size)
            disk_name = run("ls /sys/block |grep x[a-zA-Z]")
            print disk_name
            disk_name = disk_name.replace(temp,"")
            disk_name = disk_name.strip(' \t\n\r')
            disk_name="/dev/"+disk_name
            sudo("mkfs -t ext4 "+disk_name)
            sudo("mount "+disk_name+" "+vol_name)
            sudo("cp /etc/fstab /etc/fstab.orig ")
            sudo("echo -e '"+disk_name+"        " + vol_name + "        ext4    defaults,nofail 0       2' >> /etc/fstab")
            sudo("mount -a")

def sym_link(instance,i_type,vol_name='/mnt'):
    with settings(host_string=instance.private_ip_address ,user=login_user ,key_filename=key_file):
        sudo("mv /var/log /var/log_original")
        sudo("mkdir /mnt/log")
        sudo("ln -s /mnt/log /var")
        sudo("mv /var/log_original/* /var/log/")
        if i_type!='www':
            sudo("mv /var/lib/"+i_type+" /var/lib/"+i_type+"_original")
            sudo("mkdir /mnt/lib")
            sudo("mkdir /mnt/lib/"+i_type)
            sudo("ln -s /mnt/lib/"+i_type+" /var/lib")
            sudo("mv /var/lib/"+i_type+"_original/* /var/lib/"+i_type)
        else :
            # creating Symbolic link of photo's directory !!!
            sudo("mkdir /mnt/profile_phiotos/")
            sudo("chmod a+w /mnt/profile_phiotos/")

''' Adding a instance to elb  '''    
def add_to_elb(instance):    
    ''' Setting up the connection  '''
    elb_conn =  boto.ec2.elb.connect_to_region(env.aws_region)

    if elb_conn is not None:
        print "Connected to EC2 region %s" % env.aws_region
    else:
        msg = "Unable to connect to Elb region %s"
        raise IOError(msg % env.aws_region)
    
    instances = [instance.id,]
    load_balancer_name = "TrulyMadlyLoadBalancer"
    elb_conn.register_instances(load_balancer_name,instances)

def sns_subscription(instance):
#def sns_subscription():
        instance_id=instance.id
        #instance_id='i-685a0ca4'
        logging.basicConfig(filename="sns-email-sub.log", level=logging.DEBUG)
        c = boto.sns.connect_to_region("ap-southeast-1")
        topicarn = config.get('aws','topicarn')
        emailaddress = config.get('aws','email_id')
        subscription = c.subscribe(topicarn, "email", emailaddress)
        
        ec2_conn = boto.ec2.connect_to_region(AWS_REGION, aws_access_key_id=AWS_KEY, aws_secret_access_key=AWS_SECRET)
        cloudwatch_conn = boto.ec2.cloudwatch.connect_to_region(AWS_REGION, aws_access_key_id=AWS_KEY, aws_secret_access_key=AWS_SECRET)
        reservations = ec2_conn.get_all_instances(filters = {'instance-id': instance_id})
        
        if reservations and reservations[0].instances:
                instance = reservations[0].instances[0]
                instance_name = instance.tags['Name']
        else:
                print "Invalid instance-id!"
                sys.exit(1)
        
        ''' Defining Alarms  '''
        alarm_CPUUtilization = boto.ec2.cloudwatch.alarm.MetricAlarm(
                connection = cloudwatch_conn,
                name = instance_name + "-" + instance_id + "-CPUUtilization-status-alarm",
                metric = 'CPUUtilization',
                namespace = 'AWS/EC2',
                statistic = 'Average',
                comparison = '>=',
                description = 'status check for %s %s' % (instance_id, instance_name),
                threshold = config.get('cloudwatch','CPUUtilization_threshold'),
                period = config.get('cloudwatch','CPUUtilization_period'),
                evaluation_periods = config.get('cloudwatch','CPUUtilization_evaluation_periods'),
                dimensions = {'InstanceId':instance_id},
                alarm_actions = [TOPIC],
                ok_actions = [TOPIC],
                insufficient_data_actions = [TOPIC])
        
        alarm_NetworkIn = boto.ec2.cloudwatch.alarm.MetricAlarm(
                connection = cloudwatch_conn,
                name = instance_name + "-" + instance_id + "-NetworkIn-status-alarm",
                metric = 'NetworkIn',
                namespace = 'AWS/EC2',
                statistic = 'Average',
                comparison = '>=',
                description = 'status check for %s %s' % (instance_id, instance_name),
                threshold = config.get('cloudwatch','NetworkIn_threshold'),
                period = config.get('cloudwatch','NetworkIn_period'),
                evaluation_periods = config.get('cloudwatch','NetworkIn_evaluation_periods'),
                dimensions = {'InstanceId':instance_id},
                alarm_actions = [TOPIC],
                ok_actions = [TOPIC],
                insufficient_data_actions = [TOPIC])
        
        alarm_DiskSpaceUtilization = boto.ec2.cloudwatch.alarm.MetricAlarm(
                connection = cloudwatch_conn,
                name = instance_name + "-" + instance_id + "-DiskSpaceUtilization-status-alarm",
                metric = 'DiskSpaceUtilization',
                namespace = 'AWS/EC2',
                statistic = 'Average',
                comparison = '>=',
                description = 'status check for %s %s' % (instance_id, instance_name),
                threshold = config.get('cloudwatch','DiskSpaceUtilization_threshold'),
                period = config.get('cloudwatch','DiskSpaceUtilization_period'),
                evaluation_periods = config.get('cloudwatch','DiskSpaceUtilization_evaluation_periods'),
                dimensions = {'InstanceId':instance_id},
                alarm_actions = [TOPIC],
                ok_actions = [TOPIC],
                insufficient_data_actions = [TOPIC])
        
        alarm_MemoryUtilization = boto.ec2.cloudwatch.alarm.MetricAlarm(
                connection = cloudwatch_conn,
                name = instance_name + "-" + instance_id + "-MemoryUtilization-status-alarm",
                metric = 'MemoryUtilization',
                namespace = 'AWS/EC2',
                statistic = 'Average',
                comparison = '>=',
                description = 'status check for %s %s' % (instance_id, instance_name),
                threshold = config.get('cloudwatch','MemoryUtilization_threshold'),
                period = config.get('cloudwatch','MemoryUtilization_period'),
                evaluation_periods = config.get('cloudwatch','MemoryUtilization_evaluation_periods'),
                dimensions = {'InstanceId':instance_id},
                alarm_actions = [TOPIC],
                ok_actions = [TOPIC],
                insufficient_data_actions = [TOPIC])
        
        ''' Adding Alarms to cloud-watch metric '''
        cloudwatch_conn.put_metric_alarm(alarm_CPUUtilization)
        cloudwatch_conn.put_metric_alarm(alarm_NetworkIn)
        cloudwatch_conn.put_metric_alarm(alarm_DiskSpaceUtilization)
        cloudwatch_conn.put_metric_alarm(alarm_MemoryUtilization)


class Instance:
    def __init__(self,ip_address):
        self.ip_address = ip_address
      
def www():
    type='www'
    description='www'
    instance = provision_instance(type,description)
    
    with settings(host_string=instance.private_ip_address ,user=login_user ,key_filename =key_file):
        run("mkdir -p /code")
        mount(instance, type, config.get(type ,'ebs_for_code'),'/code')
        
        #Assuming AMI contains /var/www/html
        run("ln -s /code /var/www/html")
        run("cd /var/www/html/")
        run("mkdir config")
        run("mkdir trulymadly")
   
        run("svn  co https://dev.trulymadly.com/svn/trulymadly trulymadly")
        put("www/config.ini", "/var/www/html/config/config.ini")        
        sudo("chmod -R a+w /code/trulymadly/include/../lib/smarty/smarty_dir/templates_c/")

            
        #vim config/config.ini
  
        #sudo("systemctl enable httpd.service")
    
        #vim /etc/httpd/conf/httpd.conf 
   
        #sudo("systemctl start httpd.service")
        
        #apachectl 
  
        #run("systemctl restart httpd.service")
        sudo("systemctl restart httpd.service")
        sudo("apachectl -v")
        
def mysql():
        type='mysql'
        description='mysql'
      #  instance = provision_instance(type,description)  
        instance=Instance('172.31.68.161')
      #  instance.ip_address='54.169.137.119'
        master_log = ''
        master_log_pos = ''
        replication_user=config.get(type ,'replication_user')
        replication_password=config.get(type ,'replication_password')
        
        host_for_copy=config.get(type ,'ip_for_sql_dump')
        with settings(host_string=host_for_copy, user=login_user , key_filename = key_file):
            #TODO: need to add this  chmod -R a+w tmp
                file_path = config.get('mysql' ,'slave_tmp_location')
              #  print ("Enter mysql User and Password of Slave for taking backup !!")
                sudo("/etc/init.d/mysqld stop")
                sudo("tar -chzvf "+file_path+" -P /var/lib/mysql")
              #  sudo("tar -czvf "+file_path+" -P /var/lib/mysql")
       # with settings(host_string='localhost'):
        copy_mysql_dump="scp -3 -i "+key_file+" "+login_user+"@"+host_for_copy+":"+file_path+ " "+login_user+"@"+instance.ip_address+":/mnt/data.tar.gz"
        print copy_mysql_dump
        local(copy_mysql_dump)
        with settings(host_string=config.get(type ,'ip_for_sql_dump'), user=login_user , key_filename = key_file):
                sudo("/etc/init.d/mysqld start")
        with settings(host_string=instance.ip_address , user=login_user , key_filename = key_file):
                user = "root"
                print ("Enter mysql password: ")
                password = "abc123" #raw_input()
                sudo("mkdir -p /mnt/mysql")
                sudo("tar -zxvf /mnt/data.tar.gz -C /mnt/mysql")
                put('mysql/my.cnf', '/etc/my.cnf', mode=0600, use_sudo=True)
                sudo("sed -i '/server-id/ c\server-id = "+config.get(type,'slave_server_id')+"' /etc/my.cnf")
                sudo("/etc/init.d/mysqld restart")
                query ="\"CHANGE MASTER TO MASTER_USER='"+replication_user+"', MASTER_PASSWORD='"+replication_password+"';\""
                sudo("mysql -u"+user+" -p"+password+" -e "+ query)
                sudo("/etc/init.d/mysqld start")
        '''
        with settings(host_string=config.get('mysql' ,'master_ip') ,user=login_user , key_filename=key_file):
                print ("Enter mysql user and password details for Master !!\n")
                print ("Enter mysql user: ")
                user = raw_input()
                print ("Enter mysql password: ")
                password = raw_input()
                query = "\"CREATE USER '"+slave_user+"'@'"+instance.ip_address+"' IDENTIFIED BY '"+slave_password+"'; \""
                mysql_login = "mysql -u"+user+" -p"+password+" -e "
                sudo(mysql_login + query )
                query = "\"GRANT REPLICATION SLAVE ON *.* TO '"+slave_user+"'@" + instance.ip_address + "\""
                sudo(mysql_login + query)

       ''' 
                
def redis_slave():
        type='redis'
        description='redis_slave'
        instance = provision_instance(type,description)       
        with settings(host_string=instance.private_ip_address,user = login_user , key_filename = key_file):
            put(config.get('redis_slave','local_config_path'), "/tmp/redis_slave.conf")
            path="/etc/redis.conf"
            sudo("cp /tmp/redis_slave.conf "+path)
            search="slaveof <masterip> <masterport>"
            sudo("sed -i '/"+search+"/ c\ slaveof "+config.get(description,'master_ip')+" "+config.get(description,'master_port')+"' "+path)
            sudo("systemctl restart redis.service")
            sudo("systemctl enable redis.service")
            sudo("systemctl status redis.service")

