#add an import at the top
from fabric.api import *
from ConfigParser import ConfigParser
import boto.ec2
#boto.set_stream_logger('boto')
import time
import os

config = ConfigParser()
config.read ('config.ini')

env.aws_region = config.get('aws' ,'aws_region')
env.user = config.get('aws' ,'user')
env.key_filename = config.get('aws' ,'key_path')
login_user = config.get('aws' ,'user')
key_file = config.get('aws' ,'key_path')
env.timeout = config.get('aws' ,'timeout')
key_name =  config.get('aws' ,'key_name')
hosts = []

def getTags():
    conn = boto.ec2.connect_to_region(env.aws_region)
    reservations = conn.get_all_instances()
    os.rename('machines.ini','machines.ini.backup')
    f = open('machines.ini', 'w')
    for res in reservations:
        for inst in res.instances:
            if 'Name' in inst.tags and inst.state=='running':
                line=inst.id+" "+inst.tags['Name']+" "+inst.ip_address+" "+inst.private_ip_address
                print line
                f.write(line+"\n")
