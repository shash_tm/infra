def hello():
    print("Hello world!")

from fabric.api import *

#env.hosts = ['reporting_db', 'backup' ,'admin_sql', 'ad_hoc_query_machine'];

mysql_slaves = ['slave','backup','admin_sql_big','reporting_db_master','reporting_db_backup', 'ad_hoc_query_machine'];
env.user = 'root'
env.warn_only=True
env.key_filename = '/home/shash/t1.pem'
application_servers=['www1a','www1b','www','admin'];
redis_servers=['redis'];
mysql_master=['master'];
node=['node_beta','nginx_beta']

def allhosts():
 env.hosts=mysql_master+mysql_slaves+application_servers+redis_servers+node

def applications():
 env.hosts=application_servers

    
def new_pattern(pattern):
    env.hosts=[]
    #with open('machines.ini') as f:
    print pattern
    lines=open('machines.ini','r').read().split('\n')
    for line in lines:
        instance=line.split(' ' )
        if(len(instance)==4 and pattern in instance[1]):
            print instance
            env.hosts.append(instance[3])               
    
            
        
    
    

def slaves():
 env.hosts=mysql_slaves

def dbs():
 env.hosts=mysql_slaves+mysql_master

#i@parallel
def space():
   run('df -h')

def fm():
 run("free -m")

def machine():
 run('cat /proc/cpuinfo')


def ntpinstall():
 run('yum -y install ntp ntpdate ntp-doc');

def ntpinstall2():
 run('chkconfig ntpd on')
 run('/etc/init.d/ntpd start')

def ntpupdate():
 #run('/etc/init.d/ntpd stop')
 #run('ntpdate pool.ntp.org')
 run('/etc/init.d/ntpd start')

def ntpstat():
 run('ntpstat')

def uptime():
 run('uptime')

def slave_status(username,password):
  run('echo "show slave status\G" | mysql --user=%s --password=%s' %(username,password))

def runc(command):
  run('%s' %(command))

def skip_slave(username,password):
    run('echo "STOP SLAVE;SET GLOBAL SQL_SLAVE_SKIP_COUNTER=1; START SLAVE;" | mysql --user=%s --password=%s'  %(username,password))

def max_user_id(username,password):
    run('echo "select max(user_id) from user\G" | mysql --user=%s --password=%s trulymadly_prod'  %(username,password))

def query(username,password,query):
    run('echo "%s" | mysql --user=%s --password=%s trulymadly_prod'  %(query,username,password))
