# Script NEEDS 3 command line arguments.
# 1st) file to monitor, 2nd) Subject of the mail, 3rd) pattern to match.
# Script won't run without either. 
logFile=$1
subject=$2
pattern=$3
if [[ ! -n "$logFile" ]]  # Do not run if file to be monitored is not specified
then
	echo "You need to specify the log file to be monitored as first command line argument"	
else
	if [[ ! -n "$subject" ]]   # Assign a default subject if not mentioned
    then
    	subject="Error Notification:"
    fi
    if [[ ! -n "$pattern" ]]	# blank string as a grep pattern gives the whole file as output.
    then
    	pattern=""
    fi	
	serverip=`/sbin/ifconfig eth0 | grep "inet" | awk '{print $2}' | awk 'NR==1' | cut -d':' -f2`
	tailFile='/tmp/logfile'
	# use the following variables to check if service is already running or not
	flag1=`ps -ef | grep $logFile | awk '{print $9}'  | head -1`
	flag2=`ps -ef | grep $logFile | awk '{print $10}' | head -1`
	patternFound=`more $tailFile | grep "$pattern"`
	if [ "$flag1" == "-0f" ] && [ "$flag2" == "$logFile" ]
	then
 	       if [ `wc -l < $tailFile` -gt 0 ]
 	       then
    	            if [[ -n "$patternFound" ]]
        	        then
            	            echo "Subject: $2 (serverip)" | cat - $tailFile  | /usr/sbin/sendmail  -f shashwat@trulymadly.com  -t tarun@trulymadly.com
                	fi
                	> $tailFile
        	else
            	    echo "no error yet. nothing to mail"
        	fi
	else
			# tailing from 0f, so that if service is being restarted, it doesn't pick up last four lines. (already mailed)
    	    echo "starting to tail"
        	tail -0f $logFile > $tailFile &
	fi
fi
