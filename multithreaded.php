<?php

require_once dirname(__FILE__).'/S3.php';
class MyThread
{

	private $date;
	private $hour;
	private $hour1;
	private $tablename;
	private $directory;
	private $i;
	private $conn;
	private $host;
	private $user;
	private $pass;
	private $dataset;
	private $config;
	function __construct($i,$date,$hour,$hour1,$tablename,$directory,$host,$user,$pass,$dataset){
		/*$this->date=$date;
		 $this->hout=$hour;
		 $this->hour1=$hour1;
		 $this->tablename=$tablename;

		 $this->i=$i;*/
		$this->host=$host;
		$this->user=$user;
		$this->pass=$pass;
		$this->directory=$directory;
		$this->dataset=$dataset;
		
		$config_ini_file=dirname(__FILE__).'/config.ini';
		
		$this->config=parse_ini_file($config_ini_file,true);
		//var_dump($this->config);
		$this->conn=mysql_connect($host,$user,$pass);
		if ($this->conn) {
			//die('Not connected : ' . mysql_error());
			mysql_select_db('trulyamdly_prod');
		}
	
	 date_default_timezone_set('UTC');
	 if($this->dataset=='action_log')
	 {
	 	$this->GenerateCSV2($i,$date,$hour,$hour1,$tablename);
	 }
	 else if($this->dataset=='user_rec_query_log')
	 {
	 	$this->GenerateCSVQueryLog($i,$date,$hour,$hour1,$tablename);
	 }


	}
	public function saveOnS3($savefrom,$saveto){
		
		try{
			$s3 = new S3($this->config['amazon_s3']['access_key'],$this->config['amazon_s3']['secret_key']);
	
			if($s3->putObjectFile($savefrom, $this->config['amazon_s3']['bucket'], $saveto, S3::ACL_PUBLIC_READ))
				return true;
		}catch(Exception $e){
			trigger_error ($e->getMessage());
		}
	}
	
	public function UploadToS3($filepath,$tablename)
	{
		echo $filepath.'*';
		echo $tablename;
		echo PHP_EOL;
		if(!$this->saveOnS3($filepath,"files/bigquery/".$tablename))
		{
			echo 'Cant upload'.$filepath;
		}
		else 
		{
			echo 'Uploaded '.$filepath;
		}
		//echo 'Uploaded'.$x.PHP_EOL;
	}
	
	public function GenerateCSV2($i,$date,$hour,$hour1,$tablename)
	{
		$sqlquery='';
		if($i==24)
		{
			$date1 = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
			$sqlquery=("SELECT ifnull( user_id  , '') , ifnull( user_agent , '') , ifnull( ip  , '') , ifnull( activity, '') , ifnull( event_type, '') ,    ifnull( time_taken , '') , ifnull( event_status , '') , ifnull( trim(both '\"' from event_info) , '') , ifnull( source  , '') , ifnull( admin_id, '') , ifnull( tstamp  , '') , ifnull( device_id , '') , ifnull( sdk_version  , '') , ifnull( os_version , '')  INTO OUTFILE '".$this->directory."action_log_".$tablename."~$i"."'   FIELDS TERMINATED BY '\t'     FROM trulymadly_prod.action_log_new where  tstamp >='".$date." $hour"."' and tstamp < '".$date1." $hour1 '".";");
			//echo($date1);
	
		}
		else
		{
	
			$sqlquery=("SELECT ifnull( user_id  , '') , ifnull( user_agent , '') , ifnull( ip  , '') , ifnull( activity, '') , ifnull( event_type, '') ,    ifnull( time_taken , '') , ifnull( event_status , '') , ifnull( trim(both '\"' from event_info) , '') , ifnull( source  , '') , ifnull( admin_id, '') , ifnull( tstamp  , '') , ifnull( device_id , '') , ifnull( sdk_version  , '') , ifnull( os_version , '')  INTO OUTFILE '".$this->directory."action_log_".$tablename."~$i"."'  FIELDS TERMINATED BY '\t'     FROM trulymadly_prod.action_log_new where  tstamp >='".$date." $hour"."' and tstamp < '".$date." $hour1 '".";");
	
		}
		$result=mysql_query($sqlquery,$this->conn);
		echo('Zipping File Now'.$tablename.PHP_EOL);
		$output=shell_exec("gzip ".$this->directory."action_log_".$tablename."~".$i);
		echo($output.PHP_EOL.PHP_EOL);
		//$myarray[]=$this->directory."action_log_".$tablename."~".$i.'.gz';
		//$mytablename=explode('~',basename($myarray[($i-1)]));
		//echo ($sqlquery.PHP_EOL.PHP_EOL);
		$newtablename=date('Ymd',strtotime($date));
		$output1=shell_exec("/root/google-cloud-sdk/bin/bq --nosync  load --source_format=CSV  --max_bad_records=30000 -F '\t'  action_log.actionlog_$newtablename ".$this->directory."action_log_".$tablename."~".$i.".gz user_id:integer,user_agent:string,ip:string,activity:string,event_type:string,time_taken:float,event_status:string,event_info:string,source:string,admin_id:integer,tstamp:timestamp,device_id:string,sdk_version:string,os_version:string 2>&1");
		echo('Uploaded File '.$output1.PHP_EOL.PHP_EOL);
		//echo $output1;
		$this->UploadToS3($this->directory."action_log_".$tablename."~".$i.".gz",'action_log'.$tablename);
		unlink($this->directory."action_log_".$tablename."~".$i.".gz");
	}
	
	public function GenerateCSVQueryLog($i,$date,$hour,$hour1,$tablename)
	{
		$sqlquery='';
		if($i==24)
		{
			$date1 = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
			$sqlquery=("SELECT ifnull( user_id  , '') , ifnull( query , '') , ifnull( result  , '') , ifnull( tstamp, '') , ifnull( isLikeQuery, '') ,    ifnull( countToFetch , '') , ifnull(countFetched , '') , ifnull( logString  , '') INTO OUTFILE '".$this->directory."query_log_".$tablename."~$i"."'   FIELDS TERMINATED BY '\t' FROM trulymadly_prod.user_recommendation_query_log where  tstamp >='".$date." $hour"."' and tstamp < '".$date1." $hour1 '".";");
			
		
		}
		else
		{
		
			$sqlquery=("SELECT ifnull( user_id  , '') , ifnull( query , '') , ifnull( result  , '') , ifnull( tstamp, '') , ifnull( isLikeQuery, '') ,    ifnull( countToFetch , '') , ifnull( countFetched , '') , ifnull( logString  , '') INTO OUTFILE '".$this->directory."query_log_".$tablename."~$i"."'  FIELDS TERMINATED BY '\t' FROM trulymadly_prod.user_recommendation_query_log where  tstamp >='".$date." $hour"."' and tstamp < '".$date." $hour1 '".";");
		
		}
		
		echo $sqlquery.PHP_EOL.PHP_EOL;
		$result=mysql_query($sqlquery,$this->conn);
		//echo('Zipping File Now'.$tablename.PHP_EOL);
		$output=shell_exec("gzip ".$this->directory."query_log_".$tablename."~".$i);
		//echo($output.PHP_EOL.PHP_EOL);
		//$myarray[]=$this->directory."action_log_".$tablename."~".$i.'.gz';
		//$mytablename=explode('~',basename($myarray[($i-1)]));
		//echo ($sqlquery.PHP_EOL.PHP_EOL);
		$newtablename=date('Ymd',strtotime($date));
		$output1=shell_exec("/root/google-cloud-sdk/bin/bq --nosync  load --source_format=CSV  --max_bad_records=30000 -F '\t' "."query_log".".".$newtablename." ".$this->directory."query_log_".$tablename."~".$i.".gz user_id:integer,query:string,result:string,tstamp:timestamp,isLikeQuery:integer,countToFetch:integer,countFetched:integer,logString:string 2>&1");
		
		echo('Uploaded File '.$output1.PHP_EOL.PHP_EOL);
		$this->UploadToS3($this->directory."query_log_".$tablename."~".$i.".gz",'query_log'.$tablename);
		unlink($this->directory."query_log_".$tablename."~".$i.".gz");
	}
	
}

//var_dump($argv);
$instance=new MyThread($argv[1],$argv[2],$argv[3],$argv[4],$argv[5],$argv[6],$argv[7],$argv[8],$argv[9],$argv[10]);
//echo('HEllow world');
//echo($argv[1].$argv[2].$argv[3].$argv[4].$argv[5].argv[6]);
?>

