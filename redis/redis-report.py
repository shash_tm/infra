#!/usr/bin/env python
'''
Send critical Redis metrics to Amazon AWS Cloudwatch.

TODO: instead of using the credentials from ~/.aws/credentials, create an IAM role for the machine which allows it to send its metrics to the AWS.
This file should be run as a cron (every minute) to populate the graphs on cloudwatch

'''

import sys
import re

# boto is an aws library for python
from boto.ec2 import cloudwatch
from boto.utils import get_instance_metadata
import redis

rServer = redis.Redis('localhost')

def collect_redis_data():
    metrics = {}
    # get redis metrices from the redis server 
    info = rServer.info()
   
    # all required metrices can be put in an array and their corresponding values, AWS Cloudwatch supported units are stored in values, unites keys of the main metrics dictionary
    metrics['metrics']= ['connected clients','redis memory used', 'pubsub channels']
    metrics['values'] =[info['connected_clients'],info['used_memory'],info['pubsub_channels']]
    metrics['units']= ['Count','Kilobytes','Count']
    return metrics

def send_multi_metrics(instance_id, region, metrics, namespace='EC2/Memory'):
    '''
    Send multiple metrics to CloudWatch
    metrics is expected to be a dictionary containing metrics, values, units keys.
    '''
    cw = cloudwatch.connect_to_region(region)
    cw.put_metric_data(namespace, metrics['metrics'], metrics['values'],
                       unit=metrics['units'],
                       dimensions={"InstanceId": instance_id})

if __name__ == '__main__':
    metadata = get_instance_metadata()
    instance_id = metadata['instance-id']
    region = metadata['placement']['availability-zone'][0:-1]
    metrics = collect_redis_data()
    send_multi_metrics(instance_id, region, metrics)
