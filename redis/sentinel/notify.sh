#!/bin/bash



MASTER_IP=172.31.15.105
HOST='dev.trulymadly.com/trulymadly/include/redisSentinel_config.php'
case "$1" in

	+odown)

		# log event name
		printf "\n\n ------------------------------ notification at $(date) ------------------------------- \n" >> not.out	
                printf "\n EVENT NAME : +ODOWN  \n" >> not.out
		
		# get failed server ip 
		failedip=$(echo "$2" | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b")
                printf "$2 \n Failed server  - $failedip" >> not.out
                
		# change network configuration for the failed server 
		ssh -o "StrictHostKeyChecking no"  -i /home/code/infra/redis/sentinel/akshat_key.pem root@$failedip "ifconfig eth0:1 del $MASTER_IP"
                printf "\n changed failed ip config to remove master ip from eth0:1" >> not.out
               
		# get AWS instance id for the failed server
		FAILED_INSTANCE_ID=`/usr/local/bin/aws ec2 describe-instances --filters "Name=private-ip-address,Values=$failedip" | /usr/local/bin/jsawk  'return this.Reservations[0].Instances[0].NetworkInterfaces[0].NetworkInterfaceId'`
                printf "\n failed instance id : $FAILED_INSTANCE_ID" >> not.out
                
		# unassign private ip from aws
		/usr/local/bin/aws ec2 unassign-private-ip-addresses --network-interface-id $FAILED_INSTANCE_ID --private-ip-address $MASTER_IP
                printf "\n unassigned private ip from failed instance" >> not.out
                ;;


        +switch-master)

		# log event name 
		printf "\n\n ------------------------------ notification at $(date) ------------------------------- \n" >> not.out                
		printf "\n EVENT NAME: MASTER-SWITCH " >> not.out

		# get failed and new ips from message text
                validips=$(echo "$2" | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b")
                OLD_MASTER_IP=$(sed -n 1p <<< "$validips")
                NEW_MASTER_IP=$(sed -n 2p <<< "$validips")
                printf "old master : $OLD_MASTER_IP \n"  >> not.out
                printf "new master : $NEW_MASTER_IP \n"  >> not.out

                # get new master instance id and private ip to remove (slave ip)
                NEW_MASTER_INSTANCE_ID=`/usr/local/bin/aws ec2 describe-instances --filters "Name=private-ip-address,Values=$NEW_MASTER_IP" | /usr/local/bin/jsawk  'return this.Reservations[0].Instances[0].NetworkInterfaces[0].NetworkInterfaceId'`
                SLAVE_IP_TO_REMOVE_FROM_MASTER=`/usr/local/bin/aws ec2 describe-instances --filters "Name=private-ip-address,Values=$NEW_MASTER_IP" | /usr/local/bin/jsawk  'return this.Reservations[0].Instances[0].NetworkInterfaces[0].PrivateIpAddresses[1].PrivateIpAddress'`
                printf "new master instance id $NEW_MASTER_INSTANCE_ID \n"  >> not.out
                printf "new master slave ip to remove $SLAVE_IP_TO_REMOVE_FROM_MASTER \n" >> not.out
               
	        # unassign the retrieved ip from new master and assign master ip to it
                /usr/local/bin/aws ec2 unassign-private-ip-addresses --network-interface-id $NEW_MASTER_INSTANCE_ID --private-ip-address $SLAVE_IP_TO_REMOVE_FROM_MASTER
	
		# notify server
		# curl -H "Content-Type: application/json" -X POST -d '{"setStatus":1,"status":1,"ip":"172.31.15.105"}' $HOST
               
		# assign the master ip to new master 
	        /usr/local/bin/aws ec2 assign-private-ip-addresses --network-interface-id $NEW_MASTER_INSTANCE_ID --private-ip-addresses $MASTER_IP
               
		#change network configuration of the new master. Add new ip and remove old
		ssh -o "StrictHostKeyChecking no"   -i /home/code/infra/redis/sentinel/akshat_key.pem root@$NEW_MASTER_IP "ifconfig eth0:1 del $SLAVE_IP_TO_REMOVE_FROM_MASTER; ifconfig eth0:1 $MASTER_IP netmask 255.255.240.0"

                # write the freed slave ip to a file for future use
                printf "$SLAVE_IP_TO_REMOVE_FROM_MASTER\n" >> slave.out
                ;;


        -sdown)

		# log event name
		printf "\n\n ------------------------------ notification at $(date) ------------------------------- \n" >> not.out
                printf "\N EVENT NAME : -SDOWN " >> not.out
		
		# find available slave IPs to assign to revived slave
		IFS=$'\n'
                read -a IP_ARRAY < slave.out
                
		# find the primary ip revived
		VALID_IPS=$(echo "$2" | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b")
                REVIVED_SLAVE=$(sed -n 1p <<< "$VALID_IPS")
                printf "\n slave back from dead : $REVIVED_SLAVE \n" >> not.out

		# get the first ip from array to assign it to the new revived slave
                IP_TO_ASSIGN=${IP_ARRAY[0]}
                printf " ip to assign : $IP_TO_ASSIGN - \n"  >> not.out

		# get instance id of the new slave
                REVIVED_SLAVE_INSTANCE_ID=`/usr/local/bin/aws ec2 describe-instances --filters "Name=private-ip-address,Values=$REVIVED_SLAVE" | /usr/local/bin/jsawk  'return this.Reservations[0].Instances[0].NetworkInterfaces[0].NetworkInterfaceId'`
                printf "inteface id  : $REVIVED_SLAVE_INSTANCE_ID \n" >> not.out

		# assign appropriate secondary private ip to the new slave
                /usr/local/bin/aws ec2 assign-private-ip-addresses --network-interface-id $REVIVED_SLAVE_INSTANCE_ID --private-ip-addresses $IP_TO_ASSIGN
                
		# change network configuration of this slave server 
		ssh -o "StrictHostKeyChecking no"   -i /home/code/infra/redis/sentinel/akshat_key.pem root@$REVIVED_SLAVE "ifconfig eth0:1 $IP_TO_ASSIGN netmask 255.255.240.0"
                printf "assigned ip and config to revived slave" >> not.out

		# write the remaining ips to the slave.out file
                REMAINING_IPS=$((${#IP_ARRAY[@]}-1))
                printf '%s' "${IP_ARRAY[@]:1:$REMAINING_IPS}" > slave.out
		;;

esac



